package edu.tecnasa.ecommerce.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="E_CLAIM_TYPE")
public class ClaimType implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CLAIM_TYPE")
	private String claimType;

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	
	
	
}