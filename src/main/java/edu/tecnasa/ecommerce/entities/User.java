package edu.tecnasa.ecommerce.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="E_USER")
public class User implements Identifiable, Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("UserId")
	private Long id;

	@JsonProperty("UserName")
	@Column(length=255, nullable=false, unique=true)
	private String userName;
	
	@JsonIgnore
	@Column(length=150, nullable=false)
	private String password;	
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name="E_USER_CLAIM",
		joinColumns = @JoinColumn(name="ID_USER"),
		inverseJoinColumns = @JoinColumn(name = "CLAIM_TYPE"))
	private Set<ClaimType> claims;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<ClaimType> getClaims() {
		return claims;
	}

	public void setClaims(Set<ClaimType> claims) {
		this.claims = claims;
	}
	
	

}